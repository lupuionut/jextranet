[Documentation](https://bitbucket.org/lupuionut/jextranet/wiki/Home) | [Download](https://bitbucket.org/lupuionut/jextranet/downloads/)

## Changelog
### 8.0.11 (February 2022)
* Fixed. Because the component was not loading the proper version of vue library, in the admin area, the user/group permissions were not visible.

### 8.0.10 (December 2021)
* Fixed. When using legacy template (bootstrap) in front-end, on the folders page, the pagination links are not generated.

### 8.0.9 (November 2021)
* Fixed. When using sync, if the folder name contained a dot, the folder name was incorrectly stored in the database.

### 8.0.8 (August 2020)
* New.  Option to load in the front-end the bootstrap javascript framework that comes with Joomla.

### 8.0.7 (May 2020)
* Changed. Updated jquery dependency to version 3.5.1 for front-end bootstrap template (legacy)

### 8.0.6 (May 2020)
* New. Option to set how multiple emails should be sent (CC or BCC). Until this version, in case of multiple receivers of an email notification, you could not chose if receivers should be added as a CC or BCC to the email. This addition adds a uniform way of sending multiple emails, by letting you specify if the receivers should be added as a BCC or as a CC. The default value is BCC. 

### 8.0.5 (April 2020)
* New. A back-end notification can also be used in front-end. This means that if a user uploads a file into a folder via front-end, all users with access to that folder will receive a notification.
* Changed. The extension update url has been changed.

### 8.0.4 (July 2019)
* Changed. Starting this month, J!Extranet is available free of charge. That includes also future updates. For this reason, I've removed the update token and added a support button on the about page, so that you can support me via PayPal to continue working on this extension.

### 8.0.3 (June 2019)
* Fixed. It fixes the warning on the file logs page in back-end area. 

### 8.0.2 (May 2019)
* Fixed. In certain conditions, after importing a user he would have list access to all folders.
* Changed. After creation of a new folder, we stop adding an "index.html" file inside the folder.

### 8.0.1 (February 2019)
* Fixed. There was an error, in the front-end, when trying to dowload multiple files as a zip.

### 8.0 (January 2019)
* New. Option to have up to 4 custom pages in front-end for each user.
* New. Option to trigger a notification when user uploads a new file in front-end folders/my vault area.
* New. Option to trigger a notification when user sends a new message to admin.
* New. Option to trigger a notification when admin uploads a new file in user vault via back-end.
* New. Option to automatically send an email notification along with the message from admin.
* New. Option to choose if users can delete or not files from "My vault".
* Changed. On user edit page, in back-end, permissions tab is now paginated. Also, only main folders are shown, to navigate through subfolders you must click on folder name.
* Changed. During user import process, you cannot set anymore user permissions. This must be done on user page, after the import.
* Changed. Group permissions must be set only after group creation.
* Changed. Sync process has been rewritten to provide better performace.
* Changed. Selecting folders for a notification.
* Changed. Two Factor Authentication field added to login form, in case this method is activated.
* Changed. Front-end search will query also file description.
* Changed. Front-end listing of folders & files is now paginated.
* Removed. Notifications during sync process are not possible anymore.

### 7.2.3 (July 2018)
* Fixed. Save and quit button for file editing in the admin area did not save the changes.
* Changed. Replaced "safehtml" with "JComponentHelper::filterText" in the back-end models forms.

### 7.2.2 (May 2018)
* New. Option to specify the url of folder that contains the component css/js files.
This allows you to move/change the files and keep these changes during a future update.
* New. Specify if to display counting of files from a folder.
Also, specify if to count all files, including the files from subfolders.

### 7.2.1
* New. You can define a list of file names to ignore in front-end.
* Fixed. It was not possible to remove pdf file type from the list of files to preview

### 7.2
* New. Option to show file creation date in the front-end
* New. In the list of folders, in front-end, show number of files in a folder

### 7.1
* New. Option to preview files. This will open a file in a new tab, instead of forcing the download. Since the reading of file happens in the browser, make sure you allow only file types supported by browser (for ex. pdf)
* New. Option to choose ordering direction for folders and files. You can now choose direction of ordering (ascending/descending).
* Changed. Process of importing user. You can now add the user to a group directly when you import him.
* Changed. code improvements
* Fixed. File names not visible to users without download permissions, even if list permission was set.
* Fixed. In the legacy view (front-end), the upload area was not showing.

### 7.0
* New. Recursive notifications. A recursive notification will be used for a selected folder and for any subfolder (contained by that folder), even if the subfolder has not been selected at creation time.
* New. Front-end template. The new template is based on pure css and is the default one. The old one based on bootstrap 2 is still available as an option.
* New. Front-end button "Download All" in Files area. This will download all files from current page.
* New. Front-end button "Delete All" in My Vault area. This will remove all files from current page.
* Changed. When composing a message in the backe-end, you can select a group as the receiver of message.
* Changed. Modified message formatting in back-end
* Changed. Replaced front-end upload from jquery to xhr.send(FormData)
* Fixed. Emails were not sent in html format

### 6.3
* New. option to disable MIME/file type checking
* New. option to enable/disable loading Joomla's bootstrap css files
* New. implemented the update system for the component. To enable the auto update you must add the "update token" in the "About" section from back-end. This token can be obtained from https://ionutlupu.me/your-desk/orders.html
* Changed. modified the way permissions works for a user. When a user is removed from a group, he looses all permissions inherited from group.
* Fixed. a possible issue with ordering in the front-end
* Fixed. a possible issue with loading jquery in front-end
* Fixed. a possible issue with uploading restricted files by Joomla (for example php files)
* Fixed. a bug allowing the user to access a file, even if he was denied access to the file/parent folder during that session

### 6.2
* New. option to download multiple files into a single zip file
* New. option to reply to messages
* Changed. upload maximum value increased from 500 to 5000 to avoid upload large files issues
* Fixed. reversed IP deny rule (in Options -> Front-end login)

### 6.1.1
* Fixed. unproper redirection of user after login

### 6.1
* New. notifications for MyVault back-end uploads
* New. bulk permissions: a tool that allows you to select multiple users and assign permissions to all selected users in one single step
* New. option to move one file from one folder to another (back-end area)